/* eslint-disable prettier/prettier */
import React from 'react';
import {Text} from 'react-native';

const Greeting = ({name}) => <Text>Hola, {name}!</Text>;

export default Greeting;
