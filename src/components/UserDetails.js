/* eslint-disable prettier/prettier */
import React, {useState, useEffect, useCallback} from 'react';
import {View, Text, Button, ActivityIndicator, StyleSheet} from 'react-native';
import axios from 'axios';

const UserDetails = ({userId}) => {
  const [user, setUser] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const fetchUser = useCallback(async () => {
    setLoading(true);
    setError('');
    try {
      const response = await axios.get(
        `https://jsonplaceholder.typicode.com/users/${userId}`,
      );
      setUser(response.data);
    } catch (err) {
      setError('Error al cargar los datos del usuario');
    } finally {
      setLoading(false);
    }
  }, [userId]);

  useEffect(() => {
    if (userId) {
      fetchUser();
    }
  }, [userId, fetchUser]);

  return (
    <View style={styles.container}>
      {loading && <ActivityIndicator testID="loader" />}
      {error && (
        <View>
          <Text>{error}</Text>
          <View style={styles.buttonContainer}>
            <Button
              title="Reintentar"
              onPress={fetchUser}
              testID="retry-button"
              style={styles.buttonRetry}
            />
          </View>
        </View>
      )}
      {user && (
        <View style={styles.sectionContainer}>
          <Text testID="user-name" style={styles.sectionTitle}>
            Nombre: {user.name}
          </Text>
          <Text style={styles.sectionTitle}>Usuario: {user.username}</Text>
          <Text style={styles.sectionTitle}>Email: {user.email}</Text>
          <Text style={styles.sectionTitle}>Teléfono: {user.phone}</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    alignItems: 'center',
    padding: 24,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  buttonContainer: {
    marginTop: 16,
    width: 150,
    alignSelf: 'center',
  },
  buttonRetry: {
    width: 100,
  },
});

export default UserDetails;
