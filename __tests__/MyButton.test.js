/* eslint-disable prettier/prettier */
import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import MyButton from '../src/components/MyButton';

describe('MyButton', () => {
  it('debe mostrar el título correcto', () => {
    const {getByText} = render(<MyButton title="Presioname" />);
    expect(getByText('Presioname')).toBeTruthy();
  });

  it('debe manejar el evento onPress', () => {
    const mockOnPress = jest.fn();
    const {getByText} = render(
      <MyButton title="Presioname" onPress={mockOnPress} />,
    );

    fireEvent.press(getByText('Presioname'));

    expect(mockOnPress).toHaveBeenCalled();
  });
});
