/* eslint-disable prettier/prettier */
import React from 'react';
import {render} from '@testing-library/react-native';
import Greeting from '../src/components/Greeting';

describe('Greeting', () => {
  it('muestra el saludo correcto', () => {
    const {getByText} = render(<Greeting name="Mundo" />);
    expect(getByText('Hola, Mundo!')).toBeTruthy();
  });
});
