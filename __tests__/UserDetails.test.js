/* eslint-disable prettier/prettier */
import React from 'react';
import {render, waitFor, fireEvent, act} from '@testing-library/react-native';
import axios from 'axios';
import UserDetails from '../src/components/UserDetails';
import renderer from 'react-test-renderer';

jest.mock('axios');

describe('UserDetails', () => {
  test('renderizar componente UserDetails correctamente', () => {
    const tree = renderer.create(<UserDetails />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  const mockUser = {id: 1, name: 'Leanne Graham'};

  it('muestra un loader antes de recibir la respuesta y luego muestra los detalles del usuario', async () => {
    const promise = Promise.resolve({data: mockUser});
    axios.get.mockReturnValue(promise);

    const {getByTestId} = render(<UserDetails userId={1} />);

    expect(getByTestId('loader')).toBeTruthy();

    await act(() => promise);

    const userNameElement = getByTestId('user-name');
    expect(userNameElement.props.children.join('')).toBe(
      'Nombre: Leanne Graham',
    );
  });

  it('permite al usuario reintentar después de un error', async () => {
    const errorPromise = Promise.reject(
      new Error('Error al cargar los datos del usuario'),
    );
    axios.get.mockReturnValue(errorPromise);

    const {getByText, getByTestId, queryByTestId} = render(
      <UserDetails userId={1} />,
    );

    await waitFor(() =>
      expect(getByText('Error al cargar los datos del usuario')).toBeTruthy(),
    );

    expect(queryByTestId('loader')).toBeFalsy();

    const successPromise = Promise.resolve({data: mockUser});
    axios.get.mockReturnValue(successPromise);

    fireEvent.press(getByTestId('retry-button'));

    expect(getByTestId('loader')).toBeTruthy();

    await act(() => successPromise);

    const userNameElement = getByTestId('user-name');
    expect(userNameElement.props.children.join('')).toBe(
      'Nombre: Leanne Graham',
    );
  });
});
